A random list of recommendations :grin:

# Must play GameCube games

## [The Legend of Zelda: Ocarina of Time](https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_Ocarina_of_Time) ([Redump](http://redump.org/disc/1881/))

The highest rated game of all time, and rightfully so!
It was revolutionary when it was released and 25y later the things it invented can still be found everywhere in current games.
Few games ever came close to achieving perfection, but OOT certainly is not far off.

Originally released on the N64, the GC version has slighlty better graphics and smoother gameplay.
Its the ultimate version of the game.

I recommend the German translation as it is a bit better than the English one.
Mostly because the complex emotional and thematic background is conveyed more subtley in the Ger. text.
This makes for a more fluent read and more clever use of language, compared to the more up-front / direct tone of the Eng. version.
But the English version is perfectly fine as well of course.

Here's a [fitting trailer](https://www.youtube.com/watch?v=cu_ZtGP2oeM), but from the 3DS (portable) version, which looks a bit better but is otherwise the same game.
(couldn't find a good trailer for the original N64 or GC version).

Venture to Hyrule, undo what you have done, and never forget what you did in the future: 10/10

## [The Legend of Zelda: The Wind Waker](https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_The_Wind_Waker) ([Redump](http://redump.org/disc/1699/))

A direct sequel the *Ocarina of Time*, with more refined gameplay, higher frame rate and a very distinct art style.
Tasked with the impossible, Nintendo did it again and made a game as good as OOT.
Also don't be fooled by the cutesy graphics, this game is hilarious fun at most times, but quite dark and deep at others.

Make sure to play OOT before WW, as otherwise you will be missing an entire layer of backstory and references.
And same as with OOT the Ger. translation is a bit better than the Eng. one.
Here's the [attract mode trailer](https://www.youtube.com/watch?v=BP42wusaHJI) which shows you quite a bit of gameplay.

Sail the seas, brave the storm, lift the treasure of your past, and loose your self in a world without borders 10/10

## [Animal Crossing](https://en.wikipedia.org/wiki/Animal_Crossing_(video_game)) ([Redump](http://redump.org/disc/2124/))

Its ... a life simulation ... or a therapy session ... or maybe you just have to go there to figure it out ...
Maybe [this helps](https://www.youtube.com/watch?v=w6ceVfLJFmA)?

Tom Nook wants his money back! 8/10

# Must play Wii games

## [Okami](https://en.wikipedia.org/wiki/%C5%8Ckami) ([GameTDB](https://www.gametdb.com/Wii/ROWP08))

Originally released on the PS2, the Wii version is the way to go!
Why you may ask?
Well in this game you draw with a brush and the entire game world is your canvas.
On the PS2 you only have a thumb stick, but on the Wii you have the [Wii Remote](https://en.wikipedia.org/wiki/Wii_Remote), which makes it infinitely better and more precise!

Its sort of a Zelda clone, but with very unique gameplay mechanics and an absolutely beautiful art-style!
It has super awesome and fun characters, deep story and a lot to do, all topped off with a wonderful soundtrack.
The game world is also very consistent so it has a minimum amount of immersion break!

Here's a [trailer for the HD version](https://www.youtube.com/watch?v=g3WdWPKASqI) (which is identical to the Wii version AFAIK) and a [fitting review](https://www.youtube.com/watch?v=YRFUxUT1Ed4).
And here's a [full boss battle](https://www.youtube.com/watch?v=5RJem0kPmCM) (on PS3) so you can see the mechanics in action, and also get a feel for the humor of it all.

Paint the sun, make it rise and become the wolf goddess of legend 10/10

## [Wii Sports](https://en.wikipedia.org/wiki/Wii_Sports) ([GameTDB](https://www.gametdb.com/Wii/RSPP01))

Who thought playing virtual Bowling, Tennis or Golf could be this much fun.
I guess no one until the Wii showed us how its done!

Strike! 7/10

## [Fragile Dreams: Farewell Ruins of the Moon](https://en.wikipedia.org/wiki/Fragile_Dreams:_Farewell_Ruins_of_the_Moon) ([GameTDB](https://www.gametdb.com/Wii/R2GP99))

Possibly the saddest and most beautiful Wii game out there.
As you bury the old man you have been living with, you realize that you may be the last person alive.
But what happened, the earth was once full of life?
Walk through over grown, dark ruins armed with a flash light (Wii Remote), searching for clues.
You will find the answer in the graffiti, diaries and memories of the people that died.
The souls and ghosts left behind will guide you on this unforgettable journey through the remnants of civilization.
And sometimes you may help them find peace, and learn something about life.

This game is generally not scary but does have its creepy moments.
It is a deliberately slowed down and quiet experience, where you have enough time to think about the meaning of it all.
It is quiet, but still features a touching and beautiful soundtrack, that won multiple awards over the years.
Make sure to play the game in its original Japanese version with subtitles (both US and EU Wii releases give you the option to do so).
Also ignore bad reviews, a lot of them are from the US, and this is just the wrong market for such a game.
I couldn't find good review or translated trailer, so best have a look at [this Japanese one](https://www.youtube.com/watch?v=NtGuOsuWCgg), to get a feeling for what this game is.

A slow and deeply moving experience that will haunt you for a long time 9/10

## [Super Mario Galaxy](https://en.wikipedia.org/wiki/Super_Mario_Galaxy) ([GameTDB](https://www.gametdb.com/Wii/RMGP01))

Nintendo invented and perfected the 3D platforming genre within one game [Super Mario 64](https://en.wikipedia.org/wiki/Super_Mario_64).
Over the years people and even Nintendo them selfs tried, and failed to build something as good.
But maybe *Super Mario Galaxy* is the closest they got.
Beautiful visuals, super great soundtrack and incredibly inventive gameplay are just some of the things that make this thing a must play.
This [review](https://www.youtube.com/watch?v=m3RTyMtnsgc) gets it quite right (except the SMG 2 is not as good as this one).

Unless you own a Nintendo 64, this is the platformer you have to have played 9/10

# Also highly recommended - GameCube

## [Sphinx and the Cursed Mummy](https://en.wikipedia.org/wiki/Sphinx_and_the_Cursed_Mummy) ([Redump](http://redump.org/disc/19826/))

Its a Zelda clone, but a damn good one, with a bigger focus on platforming.
A game made by a company who only did cheap licensed titles, except for this one which is their very own creation.
You can feel how every dev poured their heart into this one, and it became their moment of true greatness!
This [review by Nitro Rad](https://www.youtube.com/watch?v=SHaVfaYIoMk) perfectly sums it up.

8/10

## [Chibi-Robo!](https://en.wikipedia.org/wiki/Chibi-Robo!_(video_game)) ([Redump](http://redump.org/disc/9215/))

A crazy, fun, dark, absurd adventure game with a ton of charm!
Featuring a cursing, bipolar, drug using teddy bear, health conscious aliens, talking toys, and more.
This [review](https://www.youtube.com/watch?v=UTh1jm_0wYU) gets it quite right.
Also the German version is slightly better than the English translation again.

8/10

## [Billy Hatcher and the Giant Egg](https://en.wikipedia.org/wiki/Billy_Hatcher_and_the_Giant_Egg) ([Redump](http://redump.org/disc/6609/))

If you want crazy gameplay mechanics, a rooster suit and cute looking but deadly animal companions then this should be for you!
The [review by Nitro Rad](https://www.youtube.com/watch?v=j-4y51U8d-s) nails it.
Also make sure to read the manual or a guide before playing as not all of the mechanics are explained in game for some reason.

7/10

## [Beyond Good & Evil](https://en.wikipedia.org/wiki/Beyond_Good_%26_Evil_(video_game)) ([Redump](http://redump.org/disc/13683/))

An eco scify thriller with lots of unique stuff in it.
Wonderful atmosphere and world building, great story and soundtrack!

Have a look [here](https://www.youtube.com/watch?v=E_Vx7FlMGKA) for some details.
The GC version (as well as PS2 and Xbox) have some technical drawbacks, and if you happen to have a PS3 play the HD version of it.
But I did complete the GC version and it is perfectly fine, so no worries!

8/10

## [Star Fox Adventures](https://en.wikipedia.org/wiki/Star_Fox_Adventures) ([Redump](http://redump.org/disc/5010/))

Another really good Zelda clone, but from the UK!
Really nice looking, great soundtrack, nice story, smooth gameplay, make this thing dream to complete.
The [review by Nitro Rad](https://www.youtube.com/watch?v=f7zSFNA0V5A) nails it.
Have an awesome time, with lots of jokes in a unique world.

8/10

## [F-Zero GX](https://en.wikipedia.org/wiki/F-Zero_GX) ([Redump](http://redump.org/disc/2188/))

Its like *Pod Racing* from Star Wars Episode 1, you have to have Jedi reflexes to finish!
The whole thing is a tough-as-nails, perception twisting, racer, that manages to be great fun even if you loose.
Hope its [fast enough](https://www.youtube.com/watch?v=wN8bMvYbLTw) for you!

8/10

## [The Legend of Zelda: Twilight Princess](https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_Twilight_Princess) ([Redump](http://redump.org/disc/6178/))

Originally developed for the GC but then shifted to the Wii to be a launch title, this Zelda game has a difficult development history.
And it shows ... its a far step from OOT and WW.
Initially I played the Wii version, and it took me 3 attempts to finish it, because it annoyed me so much.
But then I tried the GC version, which was still released even though it was supposed to be the killer app for the Wii.
And this was much better, the motion controls on the Wii version were a total after thought, but with a classic game pad things were all right.
Still it has its gameplay short comings, but if you manage to get over the first 4h or so, it [gets really good](https://www.youtube.com/watch?v=bW1fu4DBMoU).

8/10

# Also highly recommended - Wii

## [The Legend of Zelda: Skyward Sword](https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_Skyward_Sword) ([GameTDB](https://www.gametdb.com/Wii/SOUP01))

Its a bit of a letdown due to game design flaws it has, but not because of its motion controls, as sometimes claimed!
This Zelda game looks amazing, and has a super great soundtrack, but that is not its main attraction.
Using the [Wii MotionPlus / Wii Remote Plus](https://en.wikipedia.org/wiki/Wii_MotionPlus) it is everything Nintendo wanted motion controls to be.
Every tilting of the controller will be translated into the exact position of your sword in the game.
And everything was designed around that to great effect (review [pt1](https://www.youtube.com/watch?v=nYEyXm3ZHt8) and [pt2](https://www.youtube.com/watch?v=JKo1PmUoUvM))!

8/10

## [Boom Blox](https://en.wikipedia.org/wiki/Boom_Blox) ([GameTDB](https://www.gametdb.com/Wii/RBKP69))

A really great and [challenging physics puzzler](https://www.youtube.com/watch?v=1a1pkl2aGr8), with great use of the Wii Remote!

7/10

## [Rayman Raving Rabbids](https://en.wikipedia.org/wiki/Rayman_Raving_Rabbids) ([GameTDB](https://www.gametdb.com/Wii/RRBP41))

Its the totally [insane](https://www.youtube.com/watch?v=RA80k_RAcpU) version of Wii Sports!

7/10 

## [Zack & Wiki: Quest for Barbaros' Treasure](https://en.wikipedia.org/wiki/Zack_%26_Wiki:_Quest_for_Barbaros%27_Treasure) ([GameTDB](https://www.gametdb.com/Wii/RTZP08))

A strange, point&click adventure with a ton of [charm and great puzzles](https://www.youtube.com/watch?v=VFE7b0eC60Y).

7/10

# Tech stuff

- [Wii models](https://en.wikipedia.org/wiki/Wii#Revisions) some more [pictures and details here](https://nerdbacon.com/wii-console-varieties-what-makes-them-different/)
- [Hacking Wii](https://wii.guide/)
  - You will need an SD card (I recommend some small card, like 4Gb or 8Gb), 
    and a USB stick or USB HDD for GC games and another one for Wii games.
    GC games need to be on a FAT32 partition and Wii stuff on an NTFS partition, so two drives needed ...
- Make sure to buy a [component cable](https://www.amazon.de/-/en/RVLAKC1/dp/B000JJRV90) for good picture
  - Your TV should have a component input, but if you want a better picture you can buy [the OSSC](https://videogameperfection.com/products/open-source-scan-converter/). 
    In any case, just make sure to enable *game mode* on your TV.
  - An alternative to the OSSC is the [RetroTINK 2X Pro Multi-Format](https://videogameperfection.com/products/retrotink-multi-format/) which has a bit different features with more convenience.
   (it does have [s-video](https://en.wikipedia.org/wiki/S-Video) and [composit](https://en.wikipedia.org/wiki/Composite_video) inputs which OSSC does not have)
  - Want even better quality, then how about the [Wii dual](https://8bitmods.com/wii-dual-hdmi-rgb/) for the ultimate solution.
  - A bit cheaper option is to hack up a [GBS 8200](https://www.ebay.de/sch/i.html?_nkw=gbs8200) with the [gbs-control](https://github.com/ramapcsx2/gbs-control) custom firmware.
    It has some great features (have a [look here](https://www.youtube.com/watch?v=fmfR0XI5czI) for a good review by RetroRGB).
    I have mixed experiences with it, if it works, its great, but with component PAL (50Hz) input it tends to have problems.
    There's also occasional sync loss which produces [tearing artifacts](https://github.com/ramapcsx2/gbs-control/issues/176).
    Putting it all together is quite a bit of work (but of course also fun), 
    and should cost you around 50-70€ (depending on how cheap you get the individual components).
    If you still want to do it you will need to solder quite a bit and also buy additional things like
    - VGA to HDMI converter
    - Power supply
    - Possibly some replacement components (see [here](https://github.com/ramapcsx2/gbs-control/wiki/Hardware-Mod-Library))
    - An [Si5351 Clock Generator](https://github.com/ramapcsx2/gbs-control/wiki/Si5351-Clock-Generator-install-notes)
    - Some kind of casing
  - You could go for the [Retrotink 5x](https://www.retrotink.com/product-page/5x-pro), which is probably [the best scaler](https://www.youtube.com/watch?v=nwNrqIjxBaA), but also pricey at around 300€.
  - An OSSC, gbs-control, or RetroTink 2x/5x will also enable you to use a computer monitor instead of a TV.
  - What ever you do, don't buy these [Wii-HDMI things](https://www.amazon.de/-/en/Adapter-Converter-Support-Charging-camera/dp/B078Y58RN9), they usually have high latency (this will feel horrible) and sometimes even mess up your colors.
    Here's a good explanation on what [these things are](https://www.youtube.com/watch?v=ZUrqWN4AcJc).
- If you want to play *Skyward Sword* then better go for the *Wii Remote Plus* with build in gyros, than the *MotionPlus* attachments.
- If you buy controllers (either GC or Wii), only buy original Nintendo stuff, the 3rd party ones don't stand a chance quality wise.
  The 10 or 15€ less are not worth it.
